lab6:
	gcc -o main -std=c99 -pedantic-errors -Wall -Werror main.c status_codes.c bmp_functions.c -lm
clean:
	rm -rf main
