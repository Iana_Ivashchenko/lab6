#include "bmp_functions.h"
#include "status_codes.h"

int main()
{
    printf("read\n");
    FILE *f = fopen("Lights.bmp", "rb");
    struct BMP input;
    printf("%s\n", get_read_status_message(readBMP(f, &input)));
    fclose(f);
    printf("read done\n");
    FILE *out = fopen("out.bmp", "wb");
    struct BMP output = rotate(input);
    printf("write\n");
    printf("%s\n", get_write_status_message(writeBMP(output, out)));
    fclose(out);
    printf("write done\n");
    return 0;
}
